# Contributor: Danilo Falcão <danilo@falcao.org>
# Maintainer: Danilo Falcão <danilo@falcao.org>
pkgname=py3-pelican
_pkgname=pelican
pkgver=4.6.0
pkgrel=0
pkgdesc="Static site generator written in Python"
options="!check" # Tests require locale support
url="https://getpelican.com"
arch="noarch"
license="AGPL-3.0-only"
depends="
	python3
	py3-feedgenerator
	py3-jinja2
	py3-pygments
	py3-docutils
	py3-tz
	py3-blinker
	py3-unidecode
	py3-dateutil
	py3-markdown
	"
makedepends="py3-setuptools"
checkdepends="py3-django"
source="$_pkgname-$pkgver.tar.gz::https://github.com/getpelican/$_pkgname/archive/$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver

prepare() {
	default_prepare

	# remove shebang from python files
	sed -i '1d' \
		pelican/tools/*.py \
		pelican/tools/templates/*.py.jinja2
}

replaces="pelican" # Backwards compatibility
provides="pelican=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="0e1fae86ab914cce59c84db97c4661a454674f5e41682785aa82088e34cc9f4170b442f292fdc25c58d4c675269aa26b0652f63ca809596a175fc544d9b5fb61  pelican-4.6.0.tar.gz"
